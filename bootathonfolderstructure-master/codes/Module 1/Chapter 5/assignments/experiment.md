<!--heading-->
# Polymer Processing Technology
____

## AIM
___
<!--italics-->
*To study the construction and working of compression molding.*
____
## THEORY
___
<!--bloackquote-->
>Compression molding is a process that involves the exposure of thermosetting materials to a particular temperature to soften it which results in high strength articles. It can be employed to develop a variety of thermoplastic, thermosetting, rubber and composite products such as bottle caps, sheets, gaskets, O-rings, seals, electrical components, automotive components etc. It uses compressive force and heat to shape a raw material by using a shaped metal mold.

Process
The compression molding process includes following stages:

1.Opening of mold and application of mold releasing agent:
This step is required to free the mold from any impurity or contamination adsorbed inside of the mold cavity to achieve good surface finished product .The mold releasing agents are used to kick out the contamination generated during the cycle. The releasing agents creates invisible layer over the mold surface and reduces friction in the contact of polymer with mold surface which gives the easy removal of finished product.

2.Loading raw material in the mold cavity:
The next step is to load the charge or raw material in the mold cavity for molding. Plastics are generally heat–insulators in nature which creates a problem of uneven heat distribution. The raw material is in direct contact with mold first and then the heat is transferred through the other section by conduction. Due to the heat-insulating nature, the curing reaction will start at the surface of the material which is in direct contact with mold. However, the lack of temperature in the bulk material results in restriction of the curing.
Preheating of raw material is done which results in the reduction of the curing time of the material and hence this increases frequency of molding. The preheating also removes the moisture or other volatile components prior to molding and reduces mold shrinkage. Though preheating is beneficial in many ways, care must be taken as excessive preheating can cure the material before it reaches the mold cavities. 3.Closing of Mold:
After loading of the raw material or charge the mold is closed by using the hydraulic compression system. To reduce wear between meeting surfaces, the mold is initially closed rapidly till both halves of mold touches each other and the speed is reduced.

4.Breathing cycle:
The evolution of volatile gases during crosslinking reaction happening in the closed mold is important and has to be considered while compression molding operation. These gases must be removed from the closed mold to avoid any defects in the molded article. The trapping of these gases in the product reduces the cross linking density and can cause blisters or ruptures. To avoid this, a breathing cycle has to be given during mold closing to allow easy escape of volatile gases from the molding compound as the raw material experiences heating from the mold and from the exothermic reaction of the resin and catalyst during cure. Hence, the breathing time is the time interval between the opening and closing of mold for removal of volatile gases.

5.Compression Cycle:
After the completion of breathing cycle, the material is held under desired temperature and pressure for certain time to complete the molding/curing (thermosetting) process which is called dwell time for curing. The curing time can be decided based on the materials used and their composition and characteristics to evaluate the curing time required for the material.

6: Opening of mold and ejection of article :
After the completion of curing step, the mold opens up and the finished article is ejected out from the mold either by automatic ejection system or by manual intervention. In some cases, if the product achieved desired stiffness for the ejection, product/s can be removed from the mold before the completion of curing reaction. The curing will continue for the next several minutes as its temperature gradually returns to ambient temperature (cooling). After the completion of this cycle the mold is again filled with a fresh set of raw material and the molding process repeated.

Temperature and Bulk Density of Compression molded raw materials:
The temperature and bulk density for some common polymeric materials (in different forms) that can be compression molded are given as follows:
___
<!--table-->
| SNo.   |    Name of the Polymer       | Range of Temperature for compression molding (oc) |
| ------------- |:-------------:| -----:|
| 1     | Phenol Formaldehyde |    160-180     |
| 2      |  Urea Formaldehyde    | 145-155  |
| 3       | General Purpose Polyester | 150-160 |
| 4     | Polypropylene    | 160-175   |
| 5 | Phenol Formaldehyde      | 160-180    |
| 6 | Tetrafluoroethylene     |    160-180 |

___
___
___



| SNo.   |    Name of the Polymer       | Bulk Density (gm/cm3) |
| ------------- |:-------------:| -----:|
| 1     | Urea Formaldehyde (Powder) |    0.577    |
| 2     | Polyamide | 0.497 |
| 3       | Polyethylene (Powder) | 0.581 |
| 4     | Polyvinyl chloride   | 0.657  |
| 5 | Phenol Formaldehyde     | 0.481    |
| 6 | Melamine formaldehyde(Liquid)     |   1.2 |
| 7 | Urea Formaldehyde (Liquid)    | 1.22    |
| 8| Polypropylene (Powder)    |   0.519|
| 9|Polystyrene (Powder)    |   0.529|

___

Applications
___

>Some of the products that can be manufactured commercially from this process are: large containers, automobile panels, battery trays, fenders, hoods, kitchen bowls and trays, dinnerware, buttons, switches, seals, gaskets, footwear sole, toy parts, hair brush handles, plates, energy meter casing, bushes, O-rings, bottle caps, recreational vehicle body panels etc.

Advantages of Compression Molding
___

1.Can process a wide range of polymers such as thermoplastic, thermosetting plastics and rubbers easily.
2.Multidirectional flow of the material enabling reduction of internal stresses.
3.Processing waste generation such as runner, gate, sprue, culls etc avoided.
4.Multicavity molding fairly easy in comparison with other processing.
5.Thin walled products molding is very popular with compression molding process since it leads to very little or no warpage.
6.Relatively inexpensive for large parts molding.
7.Less expensive process due to lower machine and mold costs.

Disadvantages of Compression molding
___

1.Not used for processing of complex articles with undercuts, small holes and side draws.
2.High Cycle time.
3.High processing cost.
4.Required high manpower with respect to other processing operations.
5.Limitations in mold depth.
6.Trimming of flash is compulsary and acts as a secondary process.

Important Formula:
___

>Charge of Raw material in the cavity(grams) = Volume of cavity(cm3 s) * Bulk density of polymer raw material (gm/cm3) + (5% excess of (Volume of cavity(cm3 s) * Bulk density of polymer raw material (gm/cm3)))
Clamping pressure requirement for compression molding (Tonnes)= Projected area of article (inch2 < )* y (tonnes/inch2)
where y is in between 27.6-41.4 MPa (1.78-2.67 Tonnes/in2)
___

## PROCEDURE
___
1.Open Simulator tab.

2.Click on "Start" button.

3.A question will appear on the screen, read and answer the question by clicking on any of the given options. Press submit after the option selection.

4.If correct answer is provided, the next question will appear. Select appropriate answer from the dropdown list. Click submit after options selection.

5.If correct answer is provided, the next question will appear. Select appropriate answer from the dropdown list. Click submit after options selection.

6.if correct answers are provided, next question will appear. Now match the experimental setup with the given options from the dropdown menu. Click submit after options selection.

7.If all the answers are correct, it will move to next question. Click on one option and press submit.

8.Select the raw material from the dropdown menu.

9.Enter the Raw material weight.

10.Input the temperature value in ℃.

11.Click and start the temperature button.

12.Input the clamping pressure value.

13.Click and start the pump button.

14.Click and start semi-automatic operation.

15.Observe the compression molding cycle of 52 seconds.

16.Click on "See Result" button.

17.If correct parameters were input, a deflashed article (article without flashes) will be shown with a success message. Click on next question.
  
18.Incorrect entered parameters will show an error based on the values entered. If error message appears, then click on ‘Try again’ to go back to the raw material selection, temperature and pressure input tab. Repeat the process by identification of wrong parameter value until the you perform Step 18 successfully.

19.Read and answer the question by clicking on any of the given options. Press submit after the option selection.

20.If correct answer is provided, a quiz based on troubleshooting of compression molding process will appear. Select appropriate answers from the dropdown list. Click submit after options selection.

21.Click on "End" to complete simulation.